package ru.krivotulov.tm.enumerated;

/**
 * Role
 *
 * @author Aleksey_Krivotulov
 */
public enum Role {
    USUAL("Usual user"),
    ADMINISTRATOR("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
