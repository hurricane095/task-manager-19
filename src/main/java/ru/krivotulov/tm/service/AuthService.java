package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IUserService;
import ru.krivotulov.tm.exception.field.LoginEmptyException;
import ru.krivotulov.tm.exception.field.PasswordEmptyException;
import ru.krivotulov.tm.exception.system.AccessDeniedException;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

/**
 * AuthService
 *
 * @author Aleksey_Krivotulov
 */
public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) {
            throw new AccessDeniedException();
        }
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login,
                         final String password,
                         final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

}
