package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.IRepository;
import ru.krivotulov.tm.api.service.IService;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.exception.entity.ModelNotFoundException;
import ru.krivotulov.tm.exception.field.IdEmptyException;
import ru.krivotulov.tm.exception.field.IndexIncorrectException;
import ru.krivotulov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

/**
 * AbstractService
 *
 * @author Aleksey_Krivotulov
 */
public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M delete(final M model) {
        if (model == null) return null;
        return repository.delete(model);
    }

    @Override
    public M deleteById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.deleteById(id);
    }

    @Override
    public M deleteByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.deleteByIndex(index);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
