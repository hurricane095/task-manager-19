package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IRepository;
import ru.krivotulov.tm.model.AbstractModel;
import ru.krivotulov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * AbstractRepository
 *
 * @author Aleksey_Krivotulov
 */
public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected List<M> models = new ArrayList<>();

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(String id) {
        for (final M model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(Integer index) {
        if (index > models.size() || models.isEmpty()) return null;
        return models.get(index);
    }

    @Override
    public M delete(M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M deleteById(String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M deleteByIndex(Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return models.size();
    }

}
