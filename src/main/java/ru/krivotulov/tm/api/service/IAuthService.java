package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.model.User;

/**
 * IAuthService
 *
 * @author Aleksey_Krivotulov
 */
public interface IAuthService {
    void login(String login, String password);

    void logout();

    User registry(String login,
                  String password,
                  String email);

    User getUser();

    String getUserId();

    boolean isAuth();

}
