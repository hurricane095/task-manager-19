package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

/**
 * IUserService
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserService extends IService<User> {

    User findByLogin(String login);

    Boolean isLoginExist(String login);

    User findByEmail(String email);

    Boolean isEmailExist(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User deleteByLogin(String login);

    User deleteByEmail(String email);

    User setPassword(String userId, String password);

    User updateUser(String userId,
                    String firstName,
                    String lastName,
                    String middleName);

}
