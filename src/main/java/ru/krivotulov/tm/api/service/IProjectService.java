package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);


}
