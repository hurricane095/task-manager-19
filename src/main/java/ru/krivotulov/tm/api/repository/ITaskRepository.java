package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAllByProjectId(String projectId);

}
