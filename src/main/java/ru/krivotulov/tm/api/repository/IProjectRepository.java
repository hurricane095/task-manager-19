package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project>{

    Project create(String name);

    Project create(String name, String description);

}
