package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserRegistryCommand
 *
 * @author Aleksey_Krivotulov
 */
public class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "User registry.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        String login = TerminalUtil.readLine();
        System.out.println("ENTER PASSWORD: ");
        String password = TerminalUtil.readLine();
        System.out.println("ENTER EMAIL: ");
        String email = TerminalUtil.readLine();
        getAuthService().registry(login, password, email);
    }

}
