package ru.krivotulov.tm.command.user;

/**
 * UserLogoutCommand
 *
 * @author Aleksey_Krivotulov
 */
public class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "user-logout";

    public static final String DESCRIPTION = "Log out.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().logout();
    }

}
