package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskUpdateByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-index";

    public static final String DESCRIPTION = "Update task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        getTaskService().updateByIndex(index, name, description);
    }

}
