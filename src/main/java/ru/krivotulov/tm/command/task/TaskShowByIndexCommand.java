package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskShowByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskShowByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-index";

    public static final String DESCRIPTION = "Show task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

}
