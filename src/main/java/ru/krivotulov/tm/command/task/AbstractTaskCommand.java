package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.DateUtil;

import java.util.List;

/**
 * AbstractTaskCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected void renderTasks(final List<Task> taskList) {
        int index = 1;
        for (Task task : taskList) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

    protected void showTask(Task task) {
        if (task == null) return;
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("ID: " + task.getId());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
        final Status status = task.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
    }

}
