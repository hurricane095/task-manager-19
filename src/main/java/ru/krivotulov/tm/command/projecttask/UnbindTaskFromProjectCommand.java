package ru.krivotulov.tm.command.projecttask;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UnbindTaskFromProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public class UnbindTaskFromProjectCommand extends AbstractProjectTaskCommand {

    public static final String NAME = "unbind-task-from-project";

    public static final String DESCRIPTION = "Unbind task from project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("TASK ID: ");
        final String taskId = TerminalUtil.readLine();
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.readLine();
        getProjectTaskService().unbindTaskFromProject(taskId, projectId);
    }

}
