package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;

/**
 * ProjectChangeStatusByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-status-by-id";

    public static final String DESCRIPTION = "Change project status by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.readLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeStatusById(id, status);
    }

}
