package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * ProjectRemoveByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-id";

    public static final String DESCRIPTION = "Remove project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Project project = getProjectService().findOneById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }

}
