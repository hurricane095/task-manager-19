package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;

/**
 * ProjectChangeStatusByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-status-by-index";

    public static final String DESCRIPTION = "Change project status by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.readLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeStatusByIndex(index, status);
    }

}
